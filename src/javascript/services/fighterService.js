class FighterService {
    addFighterHealth(fighter){
        if(fighter.finalHealth === undefined){
            fighter.finalHealth = fighter.health;
        }
        fighter.setFinalHealth = function(damage){
            damage<=this.finalHealth? this.finalHealth -= damage: this.finalHealth = 0;
        }
    }

    addFighterBlock(fighter){
        fighter.setBlock = function(isBlock){
            this.isBlock = isBlock;
        }
    }

    addFighterSuperHit(fighter){
        if(!fighter.combination){
            fighter.combination = new Set();
        }
        fighter.addCombination = function(code){
            this.combination.add(code);
        };
        fighter.deleteCombination = function(code){
            this.combination.delete(code);
        };
        fighter.clearCombination = function(){
            this.combination.clear();
        };
    }
    addFighterSuperHitPause(fighter){
        fighter.setSuperHitPause = function(isSuperHitPause){
            this.isSuperHitPause = isSuperHitPause;
        }
    }

}

export const fighterService = new FighterService();