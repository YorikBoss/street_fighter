import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter) {
    const nameElement = createElement({tagName: 'div'});
    nameElement.innerHTML = `NAME: ${fighter.name}`;
    const healthElement = createElement({tagName: 'div'});
    healthElement.innerHTML = `HEALTH: ${fighter.health}`;
    const attackElement = createElement({tagName: 'div'});
    attackElement.innerHTML = `ATTACK: ${fighter.attack}`;
    const blockElement = createElement({tagName: 'div'});
    blockElement.innerHTML = `DEFENSE: ${fighter.defense}`;
    const avatarElement = createFighterImage(fighter);

    fighterElement.append(avatarElement, nameElement, healthElement, attackElement, blockElement);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
