import { controls } from '../../constants/controls';
import {fighterService} from '../services/fighterService';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    fighterService.addFighterHealth(secondFighter);
    fighterService.addFighterBlock(firstFighter);
    fighterService.addFighterHealth(firstFighter);
    fighterService.addFighterBlock(secondFighter);
    fighterService.addFighterSuperHit(firstFighter);
    fighterService.addFighterSuperHit(secondFighter);
    fighterService.addFighterSuperHitPause(firstFighter);
    fighterService.addFighterSuperHitPause(secondFighter);

    document.addEventListener('keydown', function(event){
      onKeyDown(event, firstFighter, secondFighter, resolve)
    });
    document.addEventListener('keyup', function(event){
      onKeyUp(event, firstFighter, secondFighter)
    });
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return blockPower<hitPower? hitPower - blockPower: 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return  fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.isBlock? fighter.defense * dodgeChance: 0;
}

function onKeyDown(event, firstFighter, secondFighter, resolve) {
  const [firstKeyPlayerOne, secondKeyPlayerOne, thirdKeyPlayerOne] = controls.PlayerOneCriticalHitCombination;
  const [firstKeyPlayerTwo, secondKeyPlayerTwo, thirdKeyPlayerTwo] = controls.PlayerTwoCriticalHitCombination;
  if (event.code == controls.PlayerOneAttack && !firstFighter.isBlock) {
    setDamage(firstFighter, secondFighter);
  }
  if (event.code == controls.PlayerOneBlock) {
    firstFighter.setBlock(true);
  }
  if((event.code == firstKeyPlayerOne || event.code == secondKeyPlayerOne || event.code == thirdKeyPlayerOne) && !firstFighter.isSuperHitPause){
    setCombination(firstFighter, secondFighter, event.code, controls.PlayerOneCriticalHitCombination);
  }
  if (event.code == controls.PlayerTwoAttack && !secondFighter.isBlock) {
    setDamage(secondFighter, firstFighter);
  }
  if (event.code == controls.PlayerTwoBlock) {
    secondFighter.setBlock(true);
  }
  if((event.code == firstKeyPlayerTwo || event.code == secondKeyPlayerTwo || event.code == thirdKeyPlayerTwo) && !secondFighter.isSuperHitPause){
    setCombination(secondFighter, firstFighter, event.code, controls.PlayerTwoCriticalHitCombination);
  }

  const firstPlayerIndicator = document.querySelector("#left-fighter-indicator");
  firstPlayerIndicator.style.width=`${firstFighter.finalHealth/firstFighter.health*100}%`;

  const secondPlayerIndicator = document.querySelector("#right-fighter-indicator");
  secondPlayerIndicator.style.width=`${secondFighter.finalHealth/secondFighter.health*100}%`;

  if(firstFighter.finalHealth === 0){
    return resolve(secondFighter);
  }
  if(secondFighter.finalHealth === 0){
    return resolve(firstFighter);
  }
}

function onKeyUp(event, firstFighter, secondFighter) {
  const [firstKeyPlayerOne, secondKeyPlayerOne, thirdKeyPlayerOne] = controls.PlayerOneCriticalHitCombination;
  const [firstKeyPlayerTwo, secondKeyPlayerTwo, thirdKeyPlayerTwo] = controls.PlayerTwoCriticalHitCombination;
  
  if (event.code == controls.PlayerOneBlock) {
    firstFighter.setBlock(false);
  }
  if (event.code == controls.PlayerTwoBlock) {
    secondFighter.setBlock(false);
  }
  if(event.code == firstKeyPlayerOne || event.code == secondKeyPlayerOne || event.code == thirdKeyPlayerOne){
    firstFighter.deleteCombination(event.code);
  }
  if(event.code == firstKeyPlayerTwo || event.code == secondKeyPlayerTwo || event.code == thirdKeyPlayerTwo){
    firstFighter.deleteCombination(event.code);
  }
}

function setDamage(attacker, defender){
  const damage = getDamage(attacker, defender);
  defender.setFinalHealth(damage);
}

function setSuperDamage(attacker, defender){
  defender.setFinalHealth(attacker.attack * 2);
  attacker.setSuperHitPause(true);
  setTimeout(()=>{
    attacker.setSuperHitPause(false);
  }, 10*1000)

};

function setCombination(attacker, defender, eventCode, codes){
  attacker.addCombination(eventCode);

    for (let code of codes) { 
      if (!attacker.combination.has(code)) {
        return;
      }
    }
    attacker.clearCombination();

    setSuperDamage(attacker, defender);
}



